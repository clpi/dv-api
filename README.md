# devisa_api
## Part of the Devisa Core API

---
### News
- No news!


---
### About
- Responsible for operations falling under the broad umbrella of real-time network communications


---
### Notes


---
### Links
- [dv_api Root] **(You are here!)**:
    - [api-srv](api-srv/README.md)
        - [api-actix](api-srv/api-actix/README.md)
        - [api-tide](api-srv/api-tide/README.md)
        - [api-warp](api-srv/api-warp/README.md)
    - [api-redis](api-redis/README.md)



---
### Todo
